<?php
/**
 * Created by PhpStorm.
 * User: inhere
 * Date: 2016/3/18
 * Time: 12:49
 */

namespace app\helpers\exceptions;

/**
 * Class FileWrittenException
 * @package app\extensions\exceptions
 */
class FileWrittenException extends \RuntimeException
{
    public function __construct($msg = '', $code = 14, \Exception $previous = null)
    {
        parent::__construct($msg ? : 'The data written to the file failed!!', $code, $previous);
    }

}