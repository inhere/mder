<?php

// ANSI UTF8
header('Content-Type: text/html; Charset = UTF8');

// check env
if ( !in_array( PHP_SAPI, ['cli', 'cli-server'] ) ) {
    header('HTTP/1.1 403 Forbidden');
    exit("  403 Forbidden \n\n"
        . " current environment is CLI. \n"
        . " :( Sorry! Run this script is only allowed in the terminal environment!\n,You are not allowed to access this file.\n");
}

// fcgi doesn't have STDIN and STDOUT defined by default
defined('STDIN')     or define('STDIN', fopen('php://stdin', 'r'));
defined('STDOUT')    or define('STDOUT', fopen('php://stdout', 'w'));
error_reporting(E_ALL);

Slim::$app = $app = new \slimExt\base\ConsoleApp();

$container = $app->getContainer();
$container['config'] = function ()
{
    $cliFile    = PROJECT_PATH . '/config/console/app.yml';
    $configFile = PROJECT_PATH . '/config/config.yml';

    return \slimExt\DataCollector::make($configFile,'yml','console')
        ->loadYaml($cliFile);
};

// register command
require __DIR__ . '/commands.php';

$app->run();
