<?php


namespace app\controllers\admin;

use Slim;
use slimExt\base\Request;
use slimExt\base\Response;
use app\models\Users;

/**
 * Class User
 * @package app\controllers
 */
class User extends Base
{
    /**
     * register
     * @method GET
     * @api /admin/user
     *
     * @param Request $req
     * @param Response $res
     * @return Response
     */
    public function indexAction(Request $req, Response $res)
    {
        $viewVar = [
            'roles' => Users::roles()
        ];

        return $this->renderTwig('index', $res, $viewVar);
    }
}