<?php
/**
 * Created by PhpStorm.
 * User: inhere
 * Date: 2016/03/4
 * Time: 15:30
 */

namespace app\extensions;

use slimExt\base\Collection;
use Slim;
use slimExt\DataCollector;

/**
 * Class MderItem
 * @package app\extensions
 *
 * @property string $name
 * @property string $path
 * @property string $pageSetFile
 * @property string $pageAttrFile
 * @property string $defaultSidebarFile
 * @property string $sidebarFile
 *
 * @property array $itemSet
 * @property array $pageAttr
 */
class MderItem extends Collection
{
    // const DEFAULT_NAME = 'default';

    /**
     * page belongs to item's name.
     * @var string
     */
    protected $name = '';

    /**
     * @var array
     */
    protected $data = [

        // page belongs to item's path.

        // item Set File
        'itemSetFile' => '',

        // item settings, by parse `$this->options['itemConfig']`
        'itemSet' => [],

        // the item's sidebar data default config file
        'defaultSidebarFile' => '',

        // the item's sidebar data config file
        'sidebarFile' => '',
    ];

    protected $items = [];

    /**
     * 默认值
     * @var string
     */
    protected $defaultHandler = 'app\handlers\DefaultHandler';

    /**
     * 默认值
     * @var string
     */
    protected $defaultParser = 'app\parsers\DefaultParser';

    /**
     * 默认值
     * @var array
     */
    protected $options = [
        'path'             => '',
        'outPath'          => '',
        'assetPath'        => '',
        'defaultPage'      => 'index.md',
        'handler'          => 'app\handlers\DefaultHandler',
        'parser'           => 'app\parsers\DefaultParser',

        'layoutTpl'   => '',
        'contentTpl'   => '',
        'sidebarFile'   => 'sidebar.yml',
        'configFile'    => 'config.yml',
    ];

    public function __construct($name='')
    {
        $this->loadItemsConfig()->setName($name);

        parent::__construct();
    }

    protected function loadItemsConfig()
    {
        $items = Slim::config()->get('items');

        foreach ($items as $name => $value) {
            $this->items[$name] = is_array($value) ? $value : [];
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function loadCurrentItem()
    {
        if ( !$this->exists() ) {
            return $this;
        }

        // item config file
        $itemSettingFile = Slim::alias('@project/config/items/') . $this->name . '.yml';
        if ( is_file($itemSettingFile) ) {
            $this->set('itemSetFile', $itemSettingFile)
                 ->set('itemSet', DataCollector::parseYaml($itemSettingFile));

            // merge setting to global config
            Slim::config()->loadArray($this->itemSet);
        }

        // set the item's sidebar data default config file
        $this->set('defaultSidebarFile', $this->getPath() . DIR_SEP . $this->options['sidebarFile']);

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        if ( $name && isset($this->items[$name])) {
            $this->name = trim($name);

            $this->setOptions($this->items[$this->name]);

            $this->loadCurrentItem();
        }
    }

    /**
     * @return array
     */
    public function getEnabledItems()
    {
        return array_keys($this->items);
    }

    /**
     * @param $key
     * @param mixed $default
     * @return mixed
     */
    public function getOption($key, $default=null)
    {
        return isset($this->options[$key]) ? $this->options[$key] : $default;
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @param array $options
     */
    public function setOptions($options)
    {
        // current item options config
        $this->options = array_merge($this->options, $options);

        if ( ! ($itemPath = $this->getOption('path')) ) {
            $sourcesPath = Slim::config()->get('sources.path');

            // default path
            $this->options['path'] = $itemPath = $sourcesPath . DIR_SEP . $this->name;
        }

        if ( ! ($outPath = $this->getOption('outPath')) ) {
            $outputsPath = Slim::config()->get('outputs.path');
            // default otuput path
            $this->options['outPath'] = $itemPath = $outputsPath . DIR_SEP . $this->name;
        }

        if ( $layoutTpl = $this->options['layoutTpl'] ) {
            Slim::config()->set('layout', $layoutTpl);
        }
    }

    public function getPath()
    {
        return Slim::alias($this->options['path']);
    }

    /**
     * @return bool
     */
    public function exists()
    {
        return $this->name && $this->options['path'];
    }

    /**
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        $getter = 'get'. ucfirst($name);

        if ( method_exists($this, $getter) ) {
            return $this->$getter();
        }

        return parent::__get($name);
    }

}