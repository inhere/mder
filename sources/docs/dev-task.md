{### 
title: 开发任务
route: /docs/dev-task.html
prevPage: {title: 基本使用 , url: /docs/baseUse.html }
###}

## 开发任务

- 页面的增删改查
- 特殊`item`的处理，暂时有 `blog` `docs`
- ui 的优化改进

## 我要的功能

- blog
- docs
- taskboard
- activity
- private notice