<?php
/**
 * Created by PhpStorm.
 * User: inhere
 * Date: 2016/4/5
 * Time: 15:00
 */

namespace app\handlers;


use app\models\Contents;
use app\extensions\DataEnum;
use app\models\Categories;
use Slim;
use slimExt\base\Request;

/**
 * Class BlogHandler
 * @package app\handlers
 */
class BlogHandler extends DefaultHandler
{
    public function beforeView(Request $req)
    {
//        Slim::get('config')
    }

    public function beforeRender(&$viewVar = [])
    {
        $this->contents->category = Categories::findByPk($this->contents->categoryId)->name;
        $viewVars['statusList'] = DataEnum::getStatusList();

        self::addViewVar($viewVar);
    }

}