<?php
/**
 * Created by PhpStorm.
 * User: inhere
 * Date: 2016/3/25
 * Time: 16:46
 */

namespace app\controllers;

use app\parsers\DefaultParser;
use Slim;
use slimExt\base\Request;
use slimExt\base\Response;
use slimExt\base\Controller;

/**
 * Class Base
 * @package app\controllers
 */
class Base extends Controller
{
    protected $tplHelperClass = '\app\helpers\TplHelper';

    protected function init()
    {
        parent::init();

        $this->appendTplVar['attrs'] = DefaultParser::make()->getAttrs();
    }

    /**
     * @return array
     */
    protected function addTwigGlobalVar()
    {
        $globalVar = parent::addTwigGlobalVar();

        return $globalVar;
    }
}