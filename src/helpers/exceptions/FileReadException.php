<?php
/**
 * Created by PhpStorm.
 * User: inhere
 * Date: 2016/3/18
 * Time: 12:55
 */

namespace app\helpers\exceptions;


class FileReadException extends \RuntimeException
{
    public function __construct($msg = '', $code = 14, \Exception $previous = null)
    {
        parent::__construct($msg ? : 'Failed to read the contents of the file!!', $code, $previous);
    }

}