<?php
/**
 * Created by PhpStorm.
 * User: inhere
 * Date: 2016/2/19
 * Time: 15:30
 */

namespace app\parsers;

use app\extensions\DataEnum;
//use app\models\Articles;
use Slim;

/**
 * Class BlogParser
 * @package app\parsers
 *
 * @property-read string $outputFile
 * @property $dataFile
 */
class BlogParser extends DefaultParser
{
    /**
     * append page attribute readCount
     * @inheritdoc
     */
    public function collectPageAttr($setting, $dataFile='', $onlyReturnSettings=false)
    {
        $readCount = 0;
//        $article = Articles::findOne( sprintf('route = \'%s\'', $this->pager->getPageUrl(true)), 'id,readCount' );

        // set at file: app\extensions\handlers\BlogHandler::beforeRead()
        if ($article = Slim::get('blogArticle')) {
            $readCount = $article->readCount;
        }

        Slim::config()->set('params.readCount',$readCount);

        return parent::collectPageAttr($setting, $dataFile, $onlyReturnSettings);
    }
}
