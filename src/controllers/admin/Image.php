<?php


namespace app\controllers\admin;

use app\daos\UserDao;
use Slim;
use slimExt\base\Request;
use slimExt\base\Response;

/**
 * Class Image
 * @package app\controllers
 */
class Image extends Base
{
    /**
     * @method GET
     * @api /api/image/upload
     *
     * @param Request $req
     * @param Response $res
     * @return Response
     */
    public function indexAction(Request $req, Response $res)
    {
        $dataList = UserDao::search($req->all());

        return $res->withJson(['dataList' => $dataList]);
    }

    /**
     * @method GET
     * @api /api/image/upload
     *
     * @param Request $req
     * @param Response $res
     * @return Response
     */
    public function uploadAction(Request $req, Response $res)
    {
        $dataList = UserDao::search($req->all());

        return $res->withJson(['dataList' => $dataList]);
    }
}
