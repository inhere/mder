<?php

/**
 * @var $config \slimExt\DataCollector
 */

return [
    'displayErrorDetails' => true, // set to false in production
    'charset'       => 'UTF-8',

    // language
    'language'       => [
        'lang'     => $config->get('language', 'en'),
        'basePath' => '@resources/languages',

        'langFiles' => [
            // key => file path
            // if no file key, default use file name. e.g: app.yml -> app
            'app.yml',
            // 'contents.yml',
        ],
    ],
    'user' => [
        'identityClass' => \app\models\Users::class,
    ],

    // Whoops tool config
    'debug'         => $config->get('debug', false),
    'whoops.editor' => 'sublime', // Support click to open editor

    // enableCsrfToken: true
    'enableCsrfToken' => is_pdt_env(),

    'mdpManager' => [

    ],


    'mderDb' => [
        'debug'      => !is_pdt_env(),
        'driver'     => 'sqlite',
        'database'   => '@data/sqlites/mder.db',
        'prefix'     => 'mder_'
    ],

    // Renderer settings
    'renderer' => [
        'template_path' => PROJECT_PATH . '/resources/views/',
        'tpl_suffix' => 'php',

        // add tpl global var key.
        'global_var_key' => '_globals',
        // add tpl global var. usage at view `$_globals['name'] `. if use twig `{[ _globals.name ]}`
        'global_var_list' => [
//                'name'    => 'value',
        ]
    ],

    // Twig Renderer settings
    'twigRenderer' => [
        'tpl_path'   => PROJECT_PATH . '/resources/views',
        'tpl_suffix' => 'twig',
        'lexer_options' => [
            // 'tag_variable' => ['{[', ']}'], // default is '{{', '}}' 这里为了避免与 vue.js 的 标签冲突
        ],
        'config'     => [
            'debug'      => $config->get('debug', false),
            'cache'      => $config->get('twigCache', true) ? PROJECT_PATH . '/temp/twig' : false,
            'strict_variables' => true
        ],
        // add tpl global var key.
        'global_var_key' => '_globals',
        // add tpl global var. usage {{ _globals.name }}
        'global_var_list' => [
//                'name'    => 'value',
        ]
    ],

    // Monolog settings
    'logger' => [
        'name' => 'app-run',
        'path' => PROJECT_PATH . '/temp/logs/request/app-'.date('Ymd').'.log',
    ],

    // Monolog settings
    'dbLogger' => [
        'name' => 'app-db',
        'path' => PROJECT_PATH . '/temp/logs/database/db-'.date('Ymd').'.log',
    ],

    // Monolog settings
    'errLogger' => [
        'name' => 'app-err',
        'path' => PROJECT_PATH . '/temp/logs/error/err-'.date('Ymd').'.log',
    ],
];
