<?php
namespace app\enums;

use Slim;

/**
*
*/
abstract class UserEvent
{
    // 颜色
    public static $colorList = [
            // 0 without 无
            // 1 sapphire 宝石蓝
            // 2 blue
            // 3 cyan 青绿色
            // 4 green
            // 5 yellow
            // 6 orange 橘色
            // 7 red
            // 8 crimson 奢华红
            // 9 purple 紫色
            // 10 gray 灰色
    ];

    // 提醒间隔
    public static $remindList = [
        0 => 'no remind',
        5 => '5 mini',
    ];
}