CREATE TABLE [mder_albums] (
  [id] INTEGER(11) NOT NULL,
  [userId] INT(11) NOT NULL,
  [name] CHAR(24) NOT NULL DEFAULT '',
  [describe] VARCHAR(255) NOT NULL DEFAULT '',
  [alias] CHAR(24) NOT NULL DEFAULT '',
  [createTime] INT(11) NOT NULL);

CREATE TABLE [mder_categories] (
  [id] INTEGER NOT NULL PRIMARY KEY,
  [pid] INTEGER NOT NULL DEFAULT 0,
  [name] CHAR(16) NOT NULL DEFAULT '',
  [type] CHAR(16) NOT NULL DEFAULT '',
  [createTime] INT NOT NULL DEFAULT 0,
  [status] INT(2) NOT NULL DEFAULT 1,
  [userId] INTEGER(11) NOT NULL DEFAULT 0);

CREATE TABLE [mder_contentArchives] (
  [id] INTEGER NOT NULL PRIMARY KEY,
  [userId] INTEGER NOT NULL,
  [month] INT(6) NOT NULL,
  [createTime] INT(11) NOT NULL,
  [type] CHAR(16) NOT NULL DEFAULT 'blog',
  [amount] INT(11) NOT NULL DEFAULT 0);

CREATE TABLE [mder_contents] (
  [id] INTEGER PRIMARY KEY,
  [type] CHAR NOT NULL DEFAULT 'blog',
  [userId] INTEGER NOT NULL,
  [categoryId] INT NOT NULL DEFAULT 0,
  [title] VARCHAR(128) NOT NULL,
  [digest] VARCHAR(255) NOT NULL DEFAULT '',
  [urlAlias] VARCHAR(255) NOT NULL DEFAULT '',
  [readCount] INTEGER NOT NULL DEFAULT 0,
  [publishTime] INTEGER NOT NULL,
  [createTime] INTEGER NOT NULL,
  [updateTime] INTEGER NOT NULL DEFAULT 0,
  [status] INTEGER NOT NULL DEFAULT 1,
  [content] TEXT NOT NULL DEFAULT '',
  [keywords] VARCHAR(128) NOT NULL DEFAULT '',
  [description] VARCHAR(255) NOT NULL DEFAULT '');

CREATE TABLE [mder_contentTags] (
  [id] INTEGER NOT NULL PRIMARY KEY,
  [contentId] INTEGER(11) NOT NULL DEFAULT 0,
  [tagId] INTEGER(11) NOT NULL DEFAULT 0);

CREATE TABLE [mder_guestbook] (
  [id] INTEGER(11) NOT NULL,
  [ip] CHAR(24) NOT NULL DEFAULT '',
  [content] VARCHAR(255) NOT NULL DEFAULT '',
  [createTime] INT(11) NOT NULL DEFAULT 0,
  [nickname] CHAR(24) NOT NULL DEFAULT '',
  [siteUrl] CHAR(48) NOT NULL DEFAULT '');

CREATE TABLE [mder_photos] (
  [id] INTEGER(11) NOT NULL,
  [albumId] INT(11) NOT NULL,
  [userId] INT(11) NOT NULL,
  [thumbUrl] VARCHAR(255) NOT NULL DEFAULT '',
  [url] VARCHAR(255) NOT NULL DEFAULT '',
  [title] CHAR(24) NOT NULL DEFAULT '',
  [desc] VARCHAR(255) NOT NULL DEFAULT '',
  CONSTRAINT [sqlite_autoindex_mder_photos_1] PRIMARY KEY ([id]));

CREATE TABLE [mder_settings] (
  [name] CHAR(36) NOT NULL ON CONFLICT FAIL,
  [value] CHAR(48),
  [extra] CHAR(255));

CREATE TABLE mder_tags
(
    id INTEGER PRIMARY KEY NOT NULL,
    status INT(2) DEFAULT 1 NOT NULL,
    name CHAR(16) DEFAULT '' NOT NULL,
    alias CHAR(16) DEFAULT '' NOT NULL,
    createTime INT DEFAULT 0 NOT NULL
);

CREATE UNIQUE INDEX [uni_name] ON [mder_tags] ([name]);

CREATE TABLE [mder_users] (
  [id] INTEGER PRIMARY KEY,
  [username] CHAR(12) NOT NULL ON CONFLICT FAIL,
  [password] CHAR(64) NOT NULL ON CONFLICT FAIL,
  [createTime] INTEGER(11) NOT NULL,
  [role] TINYINT(2) NOT NULL ON CONFLICT FAIL DEFAULT 3,
  [lastLogin] INTEGER(11) DEFAULT 0,
  [avatar] VARCHAR NOT NULL DEFAULT '/assets/images/avatar/avatar5.png',
  [updateTime] INT(11) NOT NULL DEFAULT 0,
  [nickname] CHAR(12) NOT NULL DEFAULT '');