<?php
namespace app\enums;

use Slim;

/**
*
*/
abstract class CacheType
{

    const FILE_TWIG    = 'twig';
    const FILE_OUTPUT  = 'output';
    const FILE_ALL     = 'all';

    const LOG_DB     = 'db';
    const LOG_ERROR  = 'error';
    const LOG_ACCESS = 'access';
    const LOG_EVENTS = 'event';


    public static function fileTypes()
    {
        return [self::FILE_ALL, self::FILE_TWIG, self::FILE_OUTPUT];
    }

    public static function logTypes()
    {
        return [self::LOG_ACCESS, self::LOG_DB, self::LOG_ERROR, self::LOG_EVENTS];
    }
}