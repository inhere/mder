<?php
/**
 * entry file is `{project}/console`
 * register console command
 * @var $app \slimExt\base\ConsoleApp
 */

// $app->add(new \app\commands\GreetCommand);
$app->add(new \app\commands\CacheCommand);
$app->add(new \app\commands\ClearSettingCommand);
$app->add(new \app\commands\GreetCommand);
$app->add(new \app\commands\ProjectExportCommand);
$app->add(new \app\commands\ProjectImportCommand);
$app->add(new \app\commands\WsCommand);
$app->add(new \app\commands\search\BuildCommand);
$app->add(new \app\commands\search\ClearCommand);