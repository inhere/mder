<?php


namespace app\controllers;

use Slim;
use slimExt\base\Request;
use slimExt\base\Response;

/**
 * Class User
 * @package app\controllers
 */
class User extends Base
{
    protected $tplPathPrefix = 'mder';

    /**
     * register
     * @method GET
     * @api /_mder/user
     *
     * @param Request $req
     * @param Response $res
     * @return Response
     */
    public function indexAction()
    {
        if ( $this->request->isPost() ) {
            return $this->deRegister();
        }

        if ( Slim::$app->user->isLogin() ) {
            return $this->response->withRedirect( Slim::config()->get('mder.afterLoginTo', '/') );
        }

        return $this->renderTwig('index', );
    }
}