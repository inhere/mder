<?php
/**
 * @var $app \Slim\App
 */
// Routes

//use app\extensions\Executor;

// set class and action
$app->any('/test/{action}', app\controllers\Test::class);
$app->get('/', 'app\controllers\Home:index');
$app->get('/home', 'app\controllers\Home:index');

//$app->get('/blog', 'app\controllers\Blog:index');
//$app->get('/blog/{category:\w+}', 'app\controllers\Blog:category');
//$app->get('/blog/archives/{date:\d{6}}', 'app\controllers\Blog:archives');

$app->map(['GET', 'POST'],'/_mder/login',  app\controllers\SimpleAuth::class . ':login');
$app->get('/_mder/logout', app\controllers\SimpleAuth::class . ':logout');
$app->map(['GET', 'POST'], '/_mder/forget-passwd',  app\controllers\Account::class . ':forgetPasswd');
$app->map(['GET', 'POST'], '/_mder/register',  app\controllers\Account::class . ':register');

// Route groups
$app->group('/_mder', function () {
     $this->get('', app\controllers\Manager::class);
     $this->get('/manager', app\controllers\Manager::class);
     $this->any('/manager/{action}', app\controllers\Manager::class)->setName('mder-manager');

     $this->get('/editor[/{action}]', app\controllers\Editor::class);

     $this->get('/page', app\controllers\Page::class . ':index');
     $this->any('/page/{action}', app\controllers\Page::class)->setName('mder-page');

     $this->get('/chatroom', app\controllers\Chatroom::class . ':index');

//     $this->any('/source/{action}', app\controllers\Source::class)->setName('mder-source');

     $this->get('/style', app\controllers\Style::class . ':index');
     $this->any('/style/{action}', app\controllers\Style::class)->setName('mder-style');

     $this->get('/user', app\controllers\User::class . ':index');
     $this->any('/user/{action}', app\controllers\User::class)->setName('mder-user');

    // $this->any('[/{action}]', app\controllers\Manager::class)
    //      ->add(middleware\mder\AuthCheck::class);
})
// ->add(slimExt\middlewares\Permission::class)
    ->add(slimExt\middlewares\AuthCheck::class);

/*
 * show sources page
 *
 * basic
 * example: `/blog/index`
 * regex: `^/[a-zA-Z]{1}\w{1,12}(?:/[a-zA-Z]{1}\w+)*`
 *
 * allow extension and length limit.
 * example: `/blog/index.html` or `/blog/index.md`
 * regex: `^/[a-zA-Z]{1}\w{1,12}(?:/[a-zA-Z]{1}\w+){0,6}(?:.html|.md)?`
 *
 * allow char '-'
 * example: `/blog/linux/how-to-install-linux.md`
 * regex: `^\/[a-zA-Z]{1}[a-zA-Z_-]{1,12}(?:\/[a-zA-Z]{1}[a-zA-Z_-]+){0,6}(?:.html|.md)?`
 * regex: `^\/[a-zA-Z]{1}[\w-]{1,12}(?:\/[a-zA-Z]{1}[\w-]+){0,6}(?:.html|.md)?`
 * regex: `^\/[a-zA-Z][\w-]{1,12}(?:\/[a-zA-Z][\w-]+){0,6}(?:.html|.md)?`
 *
 * 允许数字: e.g `/docs/go-web/zh/01.0.md`
 * regex: `^/[a-zA-Z][\w-]{1,12}(?:/[\w-]+){0,6}(?:.html|.md)?`
 */

$app->get(
    '{page:^/[a-zA-Z][\w-]{1,12}(?:/[\w-.]+){0,6}(?:.html|.md)?}',
    'app\controllers\Page:view'
);