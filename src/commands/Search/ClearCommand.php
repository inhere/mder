<?php

namespace app\commands\search;

use Slim;
use app\enums\CacheType;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * clear搜索索引文件
 * jump to the project root directory. run:
 * `./console search:indexer {type}`
 * see help: `./console search:indexer --help`
 */
class ClearCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('search:indexer')
            ->setDescription('Create search index for project')
            ->addArgument(
                'c',
                InputArgument::REQUIRED,
                'Who do you want to clear search of type?'
            )
            ->addOption(
               'yell',
               null,
               InputOption::VALUE_NONE,
               'If set, the task will yell in uppercase letters'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $type = $input->getArgument('type');
de(Slim::config());
        if ( $type === CacheType::FILE_TWIG ) {
            $re = exec('rm -rf ' . $twigPath, $outputInfo);
        } elseif ($type === CacheType::FILE_OUTPUT ) {
            $re = exec('rm -rf ' . $outputsPath, $outputInfo);
        } else {
            $re = exec('rm -rf ' . $twigPath, $outputInfo);
            $re = exec('rm -rf ' . $outputsPath, $outputInfo);
        }

        if ($input->getOption('yell')) {
            $text = strtoupper($text);
        }

        $output->writeln($text);
    }
}