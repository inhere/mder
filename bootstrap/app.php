<?php
/**
 * @var $app        \Slim\App
 * @var $container  \Slim\Container
 * @var $config     \slimExt\DataCollector
 */

// start session
session_start();

// load yaml config
$envFile    = dirname(__DIR__) . '/config/env.yml';
$configFile = dirname(__DIR__) . '/config/config.yml';
$config = \slimExt\DataCollector::make($configFile, 'yml', 'app')
        ->loadYaml(is_file($envFile) ? $envFile : '');// runtime env config

// Some setting
date_default_timezone_set($config->get('timeZone'));
define('RUNTIME_ENV', $config->get('env', PDT_RUNTIME_ENV) );

// Instantiate the container
$settings  = require dirname(__DIR__) . '/config/settings.php';
// Register dependencies
$services = require __DIR__ . '/dependencies.php';
$container = new \slimExt\base\Container($settings, $services);
$container['config'] = $config;

// Instantiate the app
//$app = new \Slim\App($settings);
Slim::$app = $app = new \slimExt\base\App($container);
Slim::alias([
    '@sources'   => PROJECT_PATH . '/sources',
    '@config'   => PROJECT_PATH . '/config',
]);

// Register middleware
require __DIR__ . '/middleware.php';

// Register routes
require __DIR__ . '/routes.php';

// Run app
$app->run();