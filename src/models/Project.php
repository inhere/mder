<?php

namespace app\models;

use Slim;
use slimExt\base\Model;
use slimExt\DataCollector;

/**
 * Class Users
 * @package app\models
 *
 * @property string $username
 * @property string $password
 * @property string $email
 * @property string $avatar
 * @property int $id
 * @property int $lastLogin
 * @property int $updateTime
 * @property int $createTime
 */
class Project extends Model
{
    public function columns()
    {
        return [
            'id'          => 'int',// 用户组id
            'name'    => 'string',
            'createrId'    => 'int',
            'logo'      => 'string',
            'email'       => 'string',
            'createTime'  => 'int',
            'updateTime'  => 'int',
        ];
    }
}
