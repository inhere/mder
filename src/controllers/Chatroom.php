<?php
/**
 * Created by PhpStorm.
 * User: inhere
 * Date: 2016/3/25
 * Time: 16:46
 */

namespace app\controllers;

use app\extensions\webSocket\WebSocket;
use Slim;
use slimExt\base\Request;
use slimExt\base\Response;

/**
 * Class Chatroom
 * you need run `./bin/console ws:run`
 * in the command window, before access this page.
 * @package app\controllers
 */
class Chatroom extends Base
{
    /**
     * @return Response
     */
    public function indexAction()
    {
        $tplVar['info'] = [
            'ip'   => '127.0.0.1',
            'port' => 8880,
            'maxConn' => 10,
        ];

        return $this->renderTwig('index',  $tplVar);
    }
}