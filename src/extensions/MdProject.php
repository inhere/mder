<?php
/**
 * Created by PhpStorm.
 * User: inhere
 * Date: 2016/03/4
 * Time: 15:30
 */

namespace app\extensions;

use slimExt\base\Collection;
use Slim;
use slimExt\DataCollector;
use inhere\librarys\StdBase;
use inhere\librarys\helpers\ObjectHelper;

/**
 * Class MdProject
 * @package app\extensions
 *
 * @property string $name
 * @property string $path
 * @property string $entryPage
 * @property string $defaultPage
 * @property string $pageAttrFile
 * @property string $defaultSidebarFile
 * @property string $sidebarFile
 *
 * @property array $projectSet
 * @property array $pageAttr
 */
class MdProject extends Collection
{
    // const DEFAULT_NAME = 'default';

    /**
     * page belongs to project's name.
     * @var string
     */
    protected $name = '';

    /**
     * @var array
     */
    protected $data = [
        'alias' => '',
        'domain' => '',

        // the project's catalog data config file
        'catalogFile' => 'catalog.yml',

        'path'             => '',
        'outPath'          => '',
        'assetPath'        => '',
        'entryPage'      => '',
        'defaultPage'      => 'index',
        'handler'          => 'app\handlers\DefaultHandler',
        'parser'           => 'app\parsers\DefaultParser',

        'layoutTpl'   => '',
        'contentTpl'   => '',
        'configFile'    => 'config.yml',
        
        'belongTo' => [
            'type' => 0
        ],

        'pageData' => [],
        'pageAsset' => [],
    ];
    
    public function __construct($name='', array $data = [])
    {
        $this->setName($name);

        parent::__construct($data);

        $this->prepare();
    }

    public function checkAccess()
    {
        // is public project.
        if ( !($bl = $this->get('belongTo')) || !isset($bl['type']) || 0 === (int)$bl['type']) {
            return true;
        }

        

    }

    /**
     * @return $this
     */
    public function prepare()
    {
        /** @var \app\extensions\ProjectManager $pm */
        $pm = Slim::get('pm');

        if ( ! ($projectPath = $this->get('path')) ) {
            // default path
            $this->data['path'] = $pm->getSourcePath() . DIR_SEP . $this->name;
        }

        if ( ! ($outPath = $this->get('outPath')) ) {
            // default output path
            $this->data['outPath'] = $pm->getOutputPath() . DIR_SEP . $this->name;
        }

        if ( $layoutTpl = $this->data['layoutTpl'] ) {
            Slim::config()->set('layout', $layoutTpl);
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        if ( $name ) {
            $this->name = trim($name);
        }
    }

    /**
     * @param array $options
     */
    public function setOptions($options)
    {
        // current project options config
        $this->options = array_merge($this->options, $options);

        if ( ! ($projectPath = $this->getOption('path')) ) {
            $sourcesPath = Slim::config()->get('sources.path');

            // default path
            $this->options['path'] = $projectPath = $sourcesPath . DIR_SEP . $this->name;
        }

        if ( ! ($outPath = $this->getOption('outPath')) ) {
            $outputsPath = Slim::config()->get('outputs.path');
            // default otuput path
            $this->options['outPath'] = $projectPath = $outputsPath . DIR_SEP . $this->name;
        }

        if ( $layoutTpl = $this->options['layoutTpl'] ) {
            Slim::config()->set('layout', $layoutTpl);
        }
    }

    public function getPath()
    {
        return Slim::alias($this->options['path']);
    }

    /**
     * @return bool
     */
    public function exists()
    {
        return $this->name && $this->data['path'];
    }

    /**
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        $getter = 'get'. ucfirst($name);

        if ( method_exists($this, $getter) ) {
            return $this->$getter();
        }

        return parent::__get($name);
    }

}
