<?php

namespace app\controllers;

// use app\parsers\DefaultParser;
use Slim;
use slimExt\base\Request;
use slimExt\base\Response;
use app\helpers\Helper;
use inhere\validate\Validation;
use app\extensions\PageEditor;

/**
 * Class Editor
 * @package app\controllers
 */
class Editor extends Base
{
    const ROOT_NODE = '#';

    /**
     * @var PageEditor
     */
    protected $pf;

    public function init()
    {
        parent::init();

        $this->pf = new PageEditor(Slim::alias('@sources/docs'));
    }

    /**
     * @return \slimExt\base\Response
     */
    public function indexAction($value='')
    {
        return $this->renderTwig('index');
    }

    /**
     * 列出目录结构
     * @return \slimExt\base\Response
     */
    public function listAction()
    {
        $id = $this->request->get('id', self::ROOT_NODE);
        $node = ($isRoot = self::ROOT_NODE === $id) ? '/' : $id;

        $rslt = $this->pf->lst($node, $isRoot);

        return $this->response->withRawJson($rslt);
    }

    /**
     * 读取文件内容
     * @return \slimExt\base\Response
     */
    public function readAction()
    {
        $id = $this->request->getTrimmed('id', self::ROOT_NODE);
        $node = ($isRoot = self::ROOT_NODE === $id) ? '/' : $id;

        $rslt = $this->pf->data($node);

        return $this->response->withRawJson($rslt);
    }

    /**
     * 创建文件/文件夹
     * @return \slimExt\base\Response
     */
    public function createAction()
    {
        $id = $this->request->getTrimmed('id', self::ROOT_NODE);
        $node = ($isRoot = self::ROOT_NODE === $id) ? '/' : $id;

        $text = $this->request->getTrimmed('text','');
        $type = $this->request->getTrimmed('type','');

        $rslt = $this->pf->create($node, $text, !$type || $type !== 'file');

        return $this->response->withRawJson($rslt);
    }

    /**
     * 更新保存文件内容
     * @return \slimExt\base\Response
     */
    public function updateAction()
    {
        $id = $this->request->get('id', self::ROOT_NODE);
        $node = ($isRoot = self::ROOT_NODE === $id) ? '/' : $id;

        $text = $this->request->getTrimmed('text','');
        $type = $this->request->getTrimmed('type','');
        $type = $this->request->get('content','');

        return $this->response->withJson($rslt);
    }

    /**
     * 重命名文件/文件夹
     * @return \slimExt\base\Response
     */
    public function renameAction()
    {
        $id = $this->request->getTrimmed('id', self::ROOT_NODE);
        $node = ($isRoot = self::ROOT_NODE === $id) ? '/' : $id;
        $text = $this->request->getTrimmed('text','');

        $rslt = $this->pf->rename($node, $text);

        return $this->response->withRawJson($rslt);
    }

    /**
     * 删除文件/文件夹
     * @return \slimExt\base\Response
     */
    public function deleteAction()
    {
        $id = $this->request->getTrimmed('id', self::ROOT_NODE);
        $node = ($isRoot = self::ROOT_NODE === $id) ? '/' : $id;
        $text = $this->request->getTrimmed('text','');

        $rslt = $this->pf->remove($node, $text);

        return $this->response->withRawJson($rslt);
    }

    /**
     * 移动文件/文件夹
     * @return \slimExt\base\Response
     */
    public function moveAction()
    {
        $id = $this->request->getTrimmed('id', self::ROOT_NODE);
        $node = ($isRoot = self::ROOT_NODE === $id) ? '/' : $id;

        $parn = $this->request->getTrimmed('parent', self::ROOT_NODE);
        $parn = (self::ROOT_NODE === $parn) ? '/' : $parn;

        $rslt = $this->pf->move($node, $parn);

        return $this->response->withRawJson($rslt);
    }

    /**
     * 拷贝文件/文件夹
     * @return \slimExt\base\Response
     */
    public function copyAction()
    {
        $id = $this->request->getTrimmed('id', self::ROOT_NODE);
        $node = ($isRoot = self::ROOT_NODE === $id) ? '/' : $id;

        $parn = $this->request->getTrimmed('parent', self::ROOT_NODE);
        $parn = (self::ROOT_NODE === $parn) ? '/' : $parn;

        $rslt = $this->pf->move($node, $parn);

        return $this->response->withRawJson($rslt);
    }
}