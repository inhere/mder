<?php

namespace app\controllers\admin;

// use app\parsers\DefaultParser;
use Slim;
use slimExt\base\Request;
use slimExt\base\Response;
use app\helpers\Helper;
use inhere\validate\Validation;

/**
 * Class Manager
 * @package app\controllers
 */
class Manager extends Base
{
    protected $tplPathPrefix = '';

    protected $enabledItems = [];

    protected function init()
    {
        parent::init();

        $this->enabledItems = Slim::config()->get('items');
    }

    public function indexAction()
    {
        $tplVar = [
            'items' => Slim::config()->get('items'),
        ];

        // return $this->sourceAction($req);

        return $this->renderTwig('index', $tplVar);
    }

    /**
     * @param Request $req
     * @param Response $res
     * @return Response
     */
    public function sourceAction()
    {
        $req = $this->request;
        $conditions = $req->getQueryParams();

        $tplVar = [
            'route'   => $req->getUri()->getPath(),
            'item'    => '',
            'path'    => $req->get('path', '', 'trim'),
            'showAll' => $req->getInt('showAll', 0),
//            'attrs' => DefaultParser::make()->getAttrs()
        ];


        return $this->renderTwig('source-file', $tplVar);
    }

    /**
     * @param $path
     * @param string $default
     * @return string
     * @internal param $suffix
     */
    protected function findItemName($path, $default='')
    {
        if (!$path) {
            return $default;
        }
        // find path belongs to which item.
        $path = trim($path,'/');
        $pos  = strpos( $path ,'/');
        // $this->item = new MderItem;

        // fix: visit `/blog` should render sidebar, it real access page is `/blog/index.md`.
        if ( $pos === false ) {
            $name = $path;
        } else {
            $name = $pos>0 ? substr($path, 0, strpos($path,'/') ) : '';
        }

        return $this->renderTwig('content-list');
    }

    /**
     * create item by zip
     */
    public function createByZipAction()
    {
        $valid = Validation::make($this->request->all(), [
            'file,name' => 'required',
        ])->validate();

        if ($valid->fail()) {
            return $this->response->withJson(__LINE__, $valid->firstError());
        }
    }

    /**
     * create item by git
     */
    public function createByGitAction()
    {

    }

    public function pageListAction()
    {
        // $newResponse = $this->response->withHeader('Content-type', 'application/json');
        d(__METHOD__,$this->request);
        return 234345;
    }

    const CACHE_TWIG = 'twig';
    const CACHE_FILE = 'file';
    const CACHE_LOG  = 'log';

    public function cacheClearAction()
    {
        $caches = [
            static::CACHE_TWIG => Slim::get('settings')['twigRenderer']['tpl_path'],
            static::CACHE_FILE => Slim::get('datConfig')->get('output.path'),
            static::CACHE_LOG  => Slim::get('settings')['logger']['path'],
        ];

        // $type = $req->getParseBody()['type'];
        $type = $this->request->getQueryParams()['type'];

        if (!$type || !isset($type) ) {
            return $this->response->withJson( __LINE__, 'param [type] is error!' );
        }

        return 234345;
    }

    public function configListAction()
    {
        $sourcesPath = Slim::config()->get('sources.path');
        $sourcesPath = Slim::alias($sourcesPath);

        $itemsConfig = Slim::config()->get('items');
        $items = [];

        foreach ($itemsConfig as $name => $options) {
            $items[] = [
                'name' => $name,
                'path' => isset($options['path']) ? Slim::alias($options['path']) : $sourcesPath.'/'.$name,
                'config' => $name . '/config.yml',
            ];
        }

        $viewVar = [
            'items'  => $items,
            'config' => 'config.yml',
            'attrs' => DefaultParser::make()->getAttrs()
        ];

        return $this->renderTwig('config', $viewVar);
    }

    public function editConfigAction()
    {
        $valid = Validation::make($this->request->all(), [
            'item,file,content' => 'required',
        ])->validate();

        if ($valid->fail()) {
            return $this->response->withJson(__LINE__, $valid->firstError());
        }

        $file = $valid->get('file');
        // $configPath = Slim::alias('@config');
        $configFile = Slim::alias('@config') . '/' . $file;

        if ( !is_file($configFile) ) {
            return $this->response->withJson(__LINE__, "config file don't exists!");
        }

        $content = file_get_contents($configFile);

        return $this->response->withJson(['content' => $content]);
    }

    public function saveConfigAction()
    {
        $valid = Validation::make($this->request->all(), [
            'item,file,content' => 'required',
        ])->validate();

        if ($valid->fail()) {
            return $this->response->withJson(__LINE__, $valid->firstError());
        }

        $file = $valid->get('file');
        // $configPath = Slim::alias('@config');
        $configFile = Slim::alias('@config') . '/' . $file;

        if ( !is_file($configFile) ) {
            return $this->response->withJson(__LINE__, "config file don't exists!");
        }
    }

    public function deleteItemAction()
    {
        $valid = Validation::make($this->request->all(), [
            'item,removeConf' => 'required',
        ])->validate();

        if ($valid->fail()) {
            return $this->response->withJson(__LINE__, $valid->firstError());
        }


    }

}