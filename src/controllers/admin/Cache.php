<?php
/**
 * Created by PhpStorm.
 * User: Inhere
 * Date: 2016/4/10 0010
 * Time: 16:04
 */

namespace app\controllers\admin;

use Slim;
use slimExt\base\Request;
use slimExt\base\Response;
use app\enums\CacheType;
use app\extensions\DataEnum;
use app\models\Users;
use app\extensions\helpers\Helper;

/**
 * Class Cache
 * @package app\controllers\admin
 */
class Cache extends Base
{
    /**
     * @method GET
     * @api /admin/cache
     *
     * @param Request $req
     * @param Response $res
     * @return Response
     */
    public function indexAction(Request $req, Response $res)
    {
        $temp = Slim::alias('@temp');

        $outputs = Helper::dirNames($temp . '/outputs');
    }

    const FILE_TWIG    = 'twig';
    const FILE_OUTPUT  = 'output';
    const FILE_ALL     = 'all';

    /**
     * @method GET
     * @api /admin/cache/file-clear
     * @args {
     *   type string in (twig output all)
     * }
     *
     * @param Request $req
     * @param Response $res
     * @return Response
     */
    public function fileClearAction(Request $req, Response $res)
    {
        $type = $req->getTrimmed('type', self::FILE_ALL );

        if ( !in_array($type, CacheType::fileTypes()) ) {
            return $res->withJson(__LINE__, 'param error!!');
        }

        $outputInfo = [];
        $temp = Slim::alias('@temp');
        $twigPath = $temp . DIR_SEP . 'twig' . DIR_SEP . '/*';
        $outputsPath = $temp . DIR_SEP . 'outputs' . DIR_SEP . '/*';

        if ( $type === self::FILE_TWIG ) {
            $re = exec('rm -rf ' . $twigPath, $outputInfo);
        } elseif ($type === self::FILE_OUTPUT ) {
            $re = exec('rm -rf ' . $outputsPath, $outputInfo);
        } else {
            $re = exec('rm -rf ' . $twigPath, $outputInfo);
            $re = exec('rm -rf ' . $outputsPath, $outputInfo);
        }

        return $res->withJson(0, 'successful!!');
    }

    const LOG_DB     = 'db';
    const LOG_ERROR  = 'error';
    const LOG_ACCESS = 'access';
    const LOG_EVENTS = 'event';

    /**
     * @method GET
     * @api /admin/cache/log-clear
     * @args {
     *   type string in (db error event access)
     * }
     *
     * @param Request $req
     * @param Response $res
     * @return Response
     */
    public function logClearAction(Request $req, Response $res)
    {
        $type = $req->getTrimmed('type', '');

        if ( !in_array($type, CacheType::logTypes()) ) {
            return $res->withJson(__LINE__, 'param error!!');
        }
    }
}