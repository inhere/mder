<?php

/**
 * Created by PhpStorm.
 * User: Inhere
 * Date: 2016/2/19 0019
 * Time: 23:35
 */

namespace app\requests;

use slimExt\ValidateRequest;

/**
 * Class PageRequest
 * @package app\requests
 */
class PageRequest extends ValidateRequest
{
    public function rules()
    {
        return [
            ['page,content', 'required'],
            ['status,digest,title', 'required', 'scene' => 'form' ],
            ['status', 'in', 'value' => [1, 2, 3, 4] ],
        ];
    }

    public function attrTrans()
    {
        return [
          'userId' => '用户Id',
        ];
    }

    // custom validator message
    public function messages()
    {
        return [
            'required' => '{attr} 是必填项。',
        ];
    }
}
