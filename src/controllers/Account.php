<?php


namespace app\controllers;

use app\models\Users;
use Slim;
use slimExt\base\Request;
use slimExt\base\Response;

/**
 * Class Account
 * @package app\controllers
 */
class Account extends Base
{
    protected $tplPathPrefix = 'mder';

    /**
     * register
     * @method GET
     * @api /_mder/register
     *
     * @return Response
     */
    public function registerAction()
    {
        if ( Slim::$app->user->isLogin() ) {
            return redirect_to( Slim::config()->get('mder.afterLoginTo', '/') );
        }

        if ( $this->request->isPost() ) {
            return $this->deRegister();
        }

        return $this->renderTwig('register');
    }

    /**
     * register
     * @method POST
     * @api /_mder/register
     *
     * @return Response
     */
    private function deRegister()
    {
        $inputs = $this->request->getParams();
        $user = new Users($inputs);
        $type = 'danger';
        $url = '/_mder/register';

        if ( ($msg = $user->register()) === true ) {
            $type = 'success';
            $msg = "hi! {$user->username}, register successful. please to email ensure register!";
            $url = '/_mder/login';
        }

        // Redirect and page alert message
        return $this->response->withRedirect($url)->withMessage(['type' => $type, 'msg' => $msg]);
    }

    /**
     * forgetPasswd
     * @method GET
     * @api /_mder/forgetPasswd
     *
     * @return Response
     */
    public function forgetPasswdAction()
    {
        if ( Slim::get('user')->isLogin() ) {
            return redirect_to( Slim::get('config')->get('mder.afterLoginTo', '/') );
        }

        if ( $this->request->isPost() ) {
            return $this->deSendEmail();
        }

        return $this->renderTwig('forget-passwd');
    }

    /**
     * Reset Passwd by email
     * @method POST
     * @api /_mder/forgetPasswd
     *
     * @return Response
     */
    private function deSendEmail()
    {
        # code...
    }

    /**
     * Reset Passwd by email
     * @method POST
     * @api /_mder/forgetPasswd
     *
     * @return Response
     */
    private function deResetPasswd()
    {
        # code...
    }
}