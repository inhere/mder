<?php
/**
 *  DIC configuration
 * @var $app        \Slim\App
 * @var $config     \slimExt\DataCollector
 * @var $services  \Slim\Container
 * @return mixed  description
 */

// custom request service {@see \Slim\DefaultServicesProvider::register()}
$services['request'] = function($c){
    /** @var $c Slim\Container */
    return \slimExt\base\Request::createFromEnvironment($c->get('environment'));
};
// custom response service {@see \Slim\DefaultServicesProvider::register()}
$services['response'] = function($c){
    /** @var $c Slim\Container */
    $headers  = new \Slim\Http\Headers(['Content-Type' => 'text/html; charset=' . $c->get('settings')['charset'] ]);
    $response = new \slimExt\base\Response(200, $headers);

    return $response->withProtocolVersion($c->get('settings')['httpVersion']);
};

// project language config
$services['language'] = function ($c) {
    /** @var $c Slim\Container */
    $options = $c->get('settings')['language'];

    return new \slimExt\base\Language($options);
};

// project login user identity
$services['user'] = function ($c) {
    /** @var $c slimExt\base\Container */
    return new \slimExt\base\User($c->get('settings')['user']);
};

// monolog - request logger
$services['logger'] = function ($c) {
    /** @var $c Slim\Container */
    $settings = $c->get('settings')['logger'];
    $logger = new \Monolog\Logger($settings['name']);
    $logger->pushProcessor(new \Monolog\Processor\UidProcessor());
    $logger->pushHandler(new \Monolog\Handler\StreamHandler($settings['path'], Monolog\Logger::DEBUG));
    return $logger;
};

// monolog - event logger
$services['eventLogger'] = function ($c) {
    /** @var $c Slim\Container */
    $settings = $c->get('settings')['eventLogger'];
    $logger = new \Monolog\Logger($settings['name']);
    // $logger->pushProcessor(new \Monolog\Processor\UidProcessor());
    $handler = new \Monolog\Handler\StreamHandler($settings['path'], Monolog\Logger::DEBUG);
    // formatter, ordering log rows
    $handler->setFormatter(new \Monolog\Formatter\LineFormatter("[%datetime%] %message% %context% \n"));
    $logger->pushHandler($handler);
    return $logger;
};

// monolog - error logger
$services['errLogger'] = function ($c) {
    /** @var $c Slim\Container */
    $settings = $c->get('settings')['errLogger'];
    $logger = new \Monolog\Logger($settings['name']);
    // $logger->pushProcessor(new \Monolog\Processor\UidProcessor());
    $logger->pushHandler(new \Monolog\Handler\StreamHandler($settings['path'], Monolog\Logger::DEBUG));
    return $logger;
};

// Register csrf Guard
$services['csrf'] = function () {
    return new \Slim\Csrf\Guard;
};

// Register flash message provider
$services['flash'] = function () {
    return new \Slim\Flash\Messages();
};

// Register Twig View helper
$services['twigRenderer'] = function ($c) {
    /** @var $c Slim\Container */
    $settings = $c->get('settings')['twigRenderer'];
    $view = new \Slim\Views\Twig($settings['tpl_path'], $settings['config']);

    // Instantiate and add Slim specific extension
    $view->addExtension(new \Slim\Views\TwigExtension( $c['router'], $c['request']->getUri() ));

    // if debug == true
    if ( isset($settings['config']['debug']) && $settings['config']['debug'] ) {
        $view->addExtension(new \Twig_Extension_Debug());
    }

    // change var tag '{{' to '{['
//    $twigEnv = $view->getEnvironment();
//    $twigEnv->setLexer( new \Twig_Lexer($twigEnv, $settings['lexer_options']) );

    return $view;
};

// php view renderer
$services['renderer'] = function ($c) {
    /** @var $c Slim\Container */
    $settings = $c->get('settings')['renderer'];
    return new \Slim\Views\PhpRenderer($settings['template_path']);
};

// // database operator
// $services['mderDb'] = function () use ($config) {
//     $options = $config->get('mderDb');
//     $options['database'] = Slim::alias($options['database']);
//     return \slimExt\database\DbFactory::getDbo('mder_db', $options);
// };

$services['pm'] = function ($c) {
    /** @var $c Slim\Container */
    $settings = $c->get('settings')['mdpManager'];
    return new \app\extensions\MdpManager($settings);
};

return $services;
