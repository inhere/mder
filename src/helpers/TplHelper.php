<?php
/**
 *
 */
namespace app\helpers;

use Slim;

class TplHelper
{
    public function getPageUrl($path, $isFolder=false)
    {
        if ( $isFolder ) {
            return $path;
        }

        if( ($suffix = get_extension($path)) && ($suffix == 'md') ) {
            $pageUrl = substr($path, 0 , - (strlen($suffix)+1));

            return $pageUrl . '.html';
        }

        return 'javascript:void(0);';
    }

    /**
     * @param Request $request
     * @return string
     */
    public function csrfField()
    {
        /** @var Slim\Csrf\Guard */
        $csrf = Slim::get('csrf');

        // CSRF token name and value
        $nameKey  = Slim::get('csrf')->getTokenNameKey();
        $valueKey = Slim::get('csrf')->getTokenValueKey();

        list($name,$value) = array_values(Slim::get('csrf')->generateToken());

        return <<<EOF
<input type="hidden" id="csrf_name" name="$nameKey" value="$name">
<input type="hidden" id="csrf_value" name="$valueKey" value="$value">
EOF;
    }

    /**
     * @param Request $request
     * @return string
     */
    public function csrfData()
    {
        return Slim::get('csrf')->generateToken();
    }

    public function __call($method, array $args=[])
    {
        if ( function_exists($method) ) {
            return call_user_func_array($method, $args);
        }
    }
}