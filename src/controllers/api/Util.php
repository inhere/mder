<?php
/**
 * Created by PhpStorm.
 * User: Inhere
 * Date: 2016/4/24 0024
 * Time: 19:27
 */

namespace app\controllers\api;

use Slim;
use slimExt\base\Request;
use slimExt\base\Response;
use inhere\tools\files\Captcha;

/**
 * Class Util
 * @package app\controllers\api
 */
class Util extends Base
{
    /**
     * @param Request $req
     * @param Response $res
     * @return static
     */
    public function captchaAction(Request $req, Response $res)
    {
        $conf = $req->getTrimmed('conf','conf1');

        Captcha::make(Slim::config('captcha.' . $conf))->show();

        return $res->withoutHeader('Content-type');
    }
}
