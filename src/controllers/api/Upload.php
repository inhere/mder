<?php
/**
 * Created by PhpStorm.
 * User: Inhere
 * Date: 2016/4/24 0024
 * Time: 22:23
 */

namespace app\controllers\api;

use Slim;
use slimExt\base\Request;
use slimExt\base\Response;
use inhere\tools\files\Uploader;

/**
 * Class FileUpload
 * @package app\controllers\api
 */
class Upload extends Base
{
    /**
     * @method GET
     * @api /api/upload/image
     *
     * @param Request $req
     * @param Response $res
     * @return Response
     */
    public function imageAction(Request $req, Response $res)
    {
        $uploader = Uploader::make()->uploadOne( 'file2', Slim::alias('@public/uploads') );

//        $files = $req->getUploadedFiles();

        de($_FILES,$uploader);

        return $res->withJson(['dataList' => $uploader->getResult() ]);
    }
}
