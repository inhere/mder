<?php

namespace app\commands;

use app\extensions\webSocket\WebSocket;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * jump to the project root directory. run:
 * `./console project:export`
 * see help: `./console project:export --help`
 * 命令行的参数是按位置赋予的
 */
class ProjectExportCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('project:export')
            // 命令描述
            ->setDescription('Export project to html files.')
            // 配置第一个参数位置，参数名 name
            // 是可选的参数
            // 参数说明
            ->addArgument(
                'name',
                InputArgument::REQUIRED,
                'Name of the will be exported project.'
            )
            ->addArgument(
                'out-path',
                InputArgument::OPTIONAL,
                'The export html files output path. Default is project path directory.'
            )
            // options
            ->addOption(
               'type',
               't',
               InputOption::VALUE_OPTIONAL,
               'If set, only export the directory files for project.',
               'html'
            )
            ->addOption(
               'dir',
               'd',
               InputOption::VALUE_OPTIONAL,
               'If set, only export the directory files for project.'
            )
            ->addOption(
               'file',
               'f',
               InputOption::VALUE_OPTIONAL,
               'If set, only export the file for project.'
            )
        ;
    }

    protected $input;
    protected $output;

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->input = $input;
        $this->output = $output;

        $ip = $input->getOption('ip');
        $port = $input->getOption('port');
        $maxConn = $input->getOption('max-conn');

        $ws = new WebSocket($ip, $port, $maxConn);

        $ws->function['add']   = [$this, 'userAddCallback'];
        $ws->function['send']  = [$this, 'sendCallback'];
        $ws->function['close'] = [$this, 'closeCallback'];

        $output->writeln("  <info>Run webSocket on the $ip:$port.</>");
        $output->writeln("  You can use <info>CTRL+C</> to exit.");
        $output->writeln("  ... ...");

        $ws->startServer();
    }

    /**************************************************************************
     * 回调函数们
     **************************************************************************/
}