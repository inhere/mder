<?php
/**
 *  DIC configuration
 * @var $app Slim\App
 * @var $container Slim\Container
 */

use slimExt\DataCollector;

$container = $app->getContainer();

// project config
$container['config'] = function () {
    $file = dirname(__DIR__) . '/config/config.yml'; // use yml
    return new DataCollector($file, DataCollector::FORMAT_YML);
};

// mder page config
$container['pageSet'] = function () {
    $file = dirname(__DIR__) . '/config/page.set.yml'; // use yml
    return new DataCollector($file, DataCollector::FORMAT_YML);
};

// mder page content config
$container['pageAttr'] = function () {
    $file = dirname(__DIR__) . '/config/page.attr.yml'; // use yml
    return new DataCollector($file, DataCollector::FORMAT_YML);
};

// project language config
$container['language'] = function ($c) {
    /** @var $c Slim\Container */
    $options = $c->get('settings')['language'];

    return new \slimExt\base\Language($options);
};

// project login's user
$container['user'] = function () {
    return new \slimExt\base\User();
};

// monolog - request logger
$container['logger'] = function ($c) {
    /** @var $c Slim\Container */
    $settings = $c->get('settings')['logger'];
    $logger = new \Monolog\Logger($settings['name']);
    $logger->pushProcessor(new \Monolog\Processor\UidProcessor());
    $logger->pushHandler(new \Monolog\Handler\StreamHandler($settings['path'], Monolog\Logger::DEBUG));
    return $logger;
};

// monolog - database logger
$container['dbLogger'] = function ($c) {
    /** @var $c Slim\Container */
    $settings = $c->get('settings')['dbLogger'];
    $logger = new \Monolog\Logger($settings['name']);
    // $logger->pushProcessor(new \Monolog\Processor\UidProcessor());
    $handler = new \Monolog\Handler\StreamHandler($settings['path'], Monolog\Logger::DEBUG);
    // formatter, ordering log rows
    $handler->setFormatter(new \Monolog\Formatter\LineFormatter("[%datetime%] SQL: %message% \n"));
    $logger->pushHandler($handler);
    return $logger;
};

// monolog - error logger
$container['errLogger'] = function ($c) {
    /** @var $c Slim\Container */
    $settings = $c->get('settings')['errLogger'];
    $logger = new \Monolog\Logger($settings['name']);
    // $logger->pushProcessor(new \Monolog\Processor\UidProcessor());
    $logger->pushHandler(new \Monolog\Handler\StreamHandler($settings['path'], Monolog\Logger::DEBUG));
    return $logger;
};

// Register csrf Guard
$container['csrf'] = function () {
    return new \Slim\Csrf\Guard;
};

// Register flash message provider
$container['flash'] = function () {
    return new \Slim\Flash\Messages();
};

// Register Twig View helper
$container['twigRenderer'] = function ($c) {
    /** @var $c Slim\Container */
    $settings = $c->get('settings')['twigRenderer'];
    $view = new \Slim\Views\Twig($settings['tpl_path'], $settings['config']);

    // Instantiate and add Slim specific extension
    $view->addExtension(new \Slim\Views\TwigExtension( $c['router'], $c['request']->getUri() ));

    // if debug == true
    if ( isset($settings['config']['debug']) && $settings['config']['debug'] ) {
        $view->addExtension(new \Twig_Extension_Debug());
    }

    // change var tag '{{' to '{['
    $twigEnv = $view->getEnvironment();
    $twigEnv->setLexer( new \Twig_Lexer($twigEnv, $settings['lexer_options']) );

    return $view;
};

// php view renderer
$container['renderer'] = function ($c) {
    /** @var $c Slim\Container */
    $settings = $c->get('settings')['renderer'];
    return new \Slim\Views\PhpRenderer($settings['template_path']);
};

// database operator
$container['mderDb'] = function ($c) {
    /** @var $c Slim\Container */
    $options = $c->get('settings')['mderDb'];
    $options['database'] = Slim::alias($options['database']);
    return \slimExt\database\DbFactory::getDbo('mder_db', $options);
};

// data file parser
$container['mderPager'] = function () {
    return new \app\extensions\MderPage();
};

// data file handler
$container['mderHandler'] = function ($c) {
    $page = $c->config->get('page');

    return \app\extensions\handlers\HandlerFactory::getHandler($page);
};