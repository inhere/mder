<?php


namespace app\controllers;

use app\daos\UserDao;
use app\models\Images;
use Slim;
use slimExt\base\Request;
use slimExt\base\Response;
use inhere\tools\files\Picture;

/**
 * Class Project
 * @package app\controllers
 */
class Project extends Base
{
    public function indexAction($value='')
    {
        $pm = Slim::$app->pm;
        
        $this->renderTwig('index');
    }

    public function createAction($value='')
    {
        # code...
    }

    public function removeAction($value='')
    {
        # code..
    }

    /**
     * create item by zip
     */
    public function createByZip()
    {
        $valid = Validation::make($req->all(), [
            'file,name' => 'required',
        ])->validate();

        if ($valid->fail()) {
            return $this->response->withJson(__LINE__, $valid->firstError());
        }
    }

    /**
     * create item by git
     */
    public function createByGit()
    {
        $valid = Validation::make($this->request->all(), [
            'url,name' => 'required',
        ])->validate();

        if ($valid->fail()) {
            return $this->response->withJson(__LINE__, $valid->firstError());
        }

        $gitUrl = $valid->get('url');

        if ( $name = $valid->get('name') ) {
            $gitUrl .= " $name";
        }

        $sourcesPath = Slim::config()->get('sources.path');
        $targetPath = $basePath = Slim::alias($sourcesPath);
        $output = [];

        exec("cd $targetPath && git clone $gitUrl", $output);

        // create config
        $configFile = Slim::alias('@config') . '/' . $name . '/config.yml';
        file_put_contents($configFile, 'name: ' . $name);

        return $this->response->withJson([
            'result'     => $output,
            'configFile' => $configFile,
        ]);
    }
}