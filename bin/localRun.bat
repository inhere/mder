@echo off

@setlocal

set IP_PORT=127.0.0.41:80
set WEB_DIR=../public

start http://"%IP_PORT%"/index.php/

set BASE_PATH=%~dp0

if "%PHP_COMMAND%" == "" set PHP_COMMAND=php.exe

"%PHP_COMMAND%" -S "%IP_PORT%" -t "%WEB_DIR%"

@endlocal
