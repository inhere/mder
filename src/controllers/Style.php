<?php

namespace app\controllers;

use slimExt\base\Request;
use slimExt\base\Response;
use Slim;

/**
 * Class Style
 * @package app\controllers
 */
class Style extends Base
{
    protected $tplPathPrefix = 'mder';

    public function indexAction()
    {
        return $this->cssThemeAction();
    }

    public function cssThemeAction()
    {
        // $bootswatchPath = Slim::alias('@public/libs/bootswatch');
        $styleList = Slim::get('config')->cssStyleList;

        return $this->renderTwig('cssStyle', [
            'bootswatchUrl' => '/assets/libs/bootswatch',
            'styleList'      => $styleList,
            'imgName'        => 'thumbnail.png',
        ]);
    }

    public function codeThemeAction()
    {
        $styleList = Slim::get('config')->codeStyleList;

    }
}