@echo off

:: -------------------------------------------------------------
::  console command line script for Windows.
::  This is the bootstrap script for running application on Windows.
:: -------------------------------------------------------------

@setlocal

TITLE Slim Application
COLOR 3f

::current dir path
SET BASE_PATH=%~dp0
SET AT_CMD=1

::php -v>nul
php -v

IF %ERRORLEVEL% NEQ 0 (
    ECHO.
    ECHO ERROR: Please send php installation directory to the environment variable PATH, to continue execution of the script
    GOTO end
)

IF "%PHP_CMD%" == "" SET PHP_CMD=php.exe

IF "%0" EQU "" (

    ECHO : ------------------------- Welcome To Use! -----------------------
    ECHO :  slim command line script for Windows.
    ECHO :  This is the bootstrap script for running application on Windows.
    ECHO :  e.g. console demo:greet name
    ECHO : -----------------------------------------------------------------
    CMD /U /E:ON /T:3f

) ELSE (
    "%PHP_CMD%" "%BASE_PATH%console" %*
)

:end
ECHO.
ECHO    exit,ByeBye!
ECHO.

@endlocal
