<?php
/**
 * Created by PhpStorm.
 * User: inhere
 * Date: 2016/3/30
 * Time: 14:49
 */

namespace app\controllers\admin;

use app\models\Contents;
use Slim;
use slimExt\base\Controller;

/**
 * Class Base
 * @package app\controllers
 */
abstract class Base extends Controller
{
    protected $tplHelperClass = 'app\\extensions\\helpers\\TplHelper';

    protected function init()
    {
        // load admin config
        Slim::config()->loadYaml(Slim::alias('@src/config/admin.yml'));
    }

    protected function addTwigGlobalVar()
    {
        $vars = parent::addTwigGlobalVar();

        return $vars;
    }
}