var simplemde = new SimpleMDE({
  element: document.getElementById("article-content"),
  autoDownloadFontAwesome : false,
  autofocus: false,
  lineWrapping: false,
  indentWithTabs: false,
  spellChecker: false,
  tabSize: 4,
  // showIcons: ["code", "table"],
  renderingConfig: {
    codeSyntaxHighlighting: true
  },
  toolbar: ["bold", "italic", "heading", "|",
    "quote","unordered-list", "ordered-list", "|",
    "code", "link", "image", "table", "|",
    "preview", "side-by-side", "fullscreen", "|",
    "guide",
    {
      name: "save-content",
      action: function sc(editor){
        // Add your own code
        console.log(editor);
        // $('#content-form').submit() -- 不会触发表单的验证
        $('button[type=submit]').click();
      },
      className: "fa fa-save fa-2x pull-right save-content",
      title: "提交保存内容"
    }
  ]
});

// simplemde.value();

var articleVM = new Vue({
  el: '#page-form',

  // 数据
  data: {
    allowStatus: GVar.allowStatus,
    settings: GVar.articleSettings,
    pageInfo: GVar.pageInfo
  },
  computed: {
    pagePath : {
      get: function() {
        return this.pageInfo.path;
      },
      set: function(val) {
        val = val.trim();

        if ( val ) {
          this.pageInfo.path = (val.substr(0,1) === '/' ? '' : '/')
              + val
              + (val.substr(-3) === '.md' ? '' : '.md');
        } else {
          this.pageInfo.path = '';
        }
      }
    },
    pageFullPath : function() {
      if (this.pageInfo.path) {
        return this.pageInfo.basePath + this.pageInfo.path;
      }

      return '';
    }
  },
  methods: {
    lcfirst: function(str){
      return GVar.lcfirst(str);
    },
    ucfirst: function(str){
      return GVar.ucfirst(str);
    },
    pageBtnUrl : function(args) {
      var url = this.pageInfo.urlPath;

      if (args && typeof args == 'object') {
        for (var key in args) {
          if (args.hasOwnProperty(key)) {
            url += (url.indexOf('?')>0 ? '&' : '?') + key + '=' + args[key];
          }
        }
      }

      return url;
    }
  }
});