<?php
/**
 * Created by PhpStorm.
 * User: Inhere
 * Date: 2016/4/10 0010
 * Time: 16:04
 */

namespace app\controllers\admin;

use Slim;
use slimExt\base\Request;
use slimExt\base\Response;
use app\extensions\DataEnum;
use app\models\Users;

/**
 * Class System
 * @package app\controllers\admin
 */
class System extends Base
{
    /**
     * @method GET
     * @api /admin/system
     *
     * @param Request $req
     * @param Response $res
     * @return Response
     */
    public function indexAction(Request $req, Response $res)
    {

    }

    /**
     * @method POST
     * @api /admin/system
     *
     * @param Request $req
     * @param Response $res
     * @return Response
     */
    public function indexAction(Request $req, Response $res)
    {

    }
}