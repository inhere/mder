<?php
/**
 * Created by PhpStorm.
 * User: inhere
 * Date: 2016/3/21
 * Time: 10:50
 */

namespace app\handlers;

use app\extensions\MderPage;
use slimExt\exceptions\NotFoundException;

/**
 * Class HandlerFactory
 * @package app\handlers
 */
abstract class HandlerFactory
{
    /**
     * @param $page
     * @param bool $isNew
     * @return AbstractHandler
     */
    public static function getHandler($page, $isNew = true)
    {
        $pager = new MderPage( trim($page) );

        $itemName = $pager->getItemName();
        $defaultHandler = __NAMESPACE__ . '\\' . ucfirst($itemName) . 'Handler';
        $handlerClass = $pager->getItem()->getOption('handler') ?: $defaultHandler ;

        if ( !class_exists($handlerClass) ) {
            throw new NotFoundException('Handler class ['.$handlerClass.'] not found!!');
        }

        return new $handlerClass($pager, $isNew);
    }
}