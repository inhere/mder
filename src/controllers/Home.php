<?php

namespace app\controllers;

use app\parsers\DefaultParser;
use Slim;
use slimExt\base\Request;
use slimExt\base\Response;

/**
 * Class Home
 * @package app
 */
class Home extends Base
{
    protected $tplPathPrefix = 'mder';

    public function init()
    {
        parent::init();

        Slim::get('config')->pageRoute = '/home';
    }

    public function indexAction()
    {
//        return $res->withRedirect('/docs');
        $items = Slim::config()->get('items');

        return $this->renderTwig('index', [
            'items' => $items,
            'attrs' => DefaultParser::make()->getAttrs()
        ]);
    }



}