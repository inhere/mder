<?php
namespace app\extensions;

use Slim;

/**
*
*/
abstract class DataEnum
{
    /*
     *  public   Open, all the people can access.        公开的,所有人可访问
     *  member   Only the login have access permissions. 仅对登录者开放
     *  private  Only allows authors to visit.           私人的,只允许作者访问
     *  draft    Only allows authors to visit,too.       草稿,只允许作者访问     *
     */

    const STATUS_PUBLIC  = 1;
    const STATUS_MEMBER  = 2;
    const STATUS_PRIVATE = 3;
    const STATUS_DRAFT   = 4;

    const PAGE_SIZE   = 10;

    const TYPE_BLOG = 'blog';
//    const TYPE_DOCS = 'docs';
    const TYPE_TASK = 'task';
    const TYPE_ACT  = 'activity';
    const TYPE_MOOD = 'mood';

    public static function getStatusList()
    {
        return [
            static::STATUS_PUBLIC  => Slim::$app->language->tran('status-public') ,
            static::STATUS_MEMBER  => Slim::$app->language->tran('status-member'),
            static::STATUS_PRIVATE => Slim::$app->language->tran('status-private'),
            static::STATUS_DRAFT   => Slim::$app->language->tran('status-draft'),
        ];
    }

    public static function getTypeList()
    {
        return [
            static::TYPE_BLOG,
            static::TYPE_TASK,
            static::TYPE_ACT,
            static::TYPE_MOOD,
        ];
    }
}