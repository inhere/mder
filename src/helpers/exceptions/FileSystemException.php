<?php
/**
 * Created by PhpStorm.
 * User: inhere
 * Date: 2016/3/18
 * Time: 12:53
 */

namespace app\helpers\exceptions;

/**
 * Class FileSystemException
 * @package app\extensions\exceptions
 */
class FileSystemException extends \RuntimeException
{
    public function __construct($msg = '', $code = 17, \Exception $previous = null)
{
    parent::__construct($msg ? : 'File system to deal with failure!!', $code, $previous);
}

}