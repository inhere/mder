<?php

namespace app\commands\search;

use Slim;
use app\enums\CacheType;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * 创建搜索索引文件
 * jump to the project root directory. run:
 * `./console search:build {type}`
 * see help: `./console search:build --help`
 */
class BuildCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('search:build')
            ->setDescription('Create search index for project')
            ->addArgument(
                'project',
                InputArgument::REQUIRED,
                'You want to create an index for that project name?'
            )
            ->addOption(
               'force',
               'f',
               InputOption::VALUE_OPTIONAL,
               'If the index already exists, it will be deleted and re created.',
               false
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $type = $input->getArgument('type');
de(Slim::config());
        if ( $type === CacheType::FILE_TWIG ) {
            $re = exec('rm -rf ' . $twigPath, $outputInfo);
        } elseif ($type === CacheType::FILE_OUTPUT ) {
            $re = exec('rm -rf ' . $outputsPath, $outputInfo);
        } else {
            $re = exec('rm -rf ' . $twigPath, $outputInfo);
            $re = exec('rm -rf ' . $outputsPath, $outputInfo);
        }

        if ($input->getOption('yell')) {
            $text = strtoupper($text);
        }

        $output->writeln($text);
    }
}