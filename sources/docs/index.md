{###
title: show code 
description: sdf dff dff
route: /docs/index
nextPage: {title: 基本使用 , url: /docs/baseUse }
###}

- php code

``` php
// Register Twig View helper
$container['view'] = function ($c) {
    $view = new \Slim\Views\Twig(PROJECT_PATH . '/templates', [
        'cache' => false,
        // 'cache' => PROJECT_PATH . '/temp/twig'
    ]);

    // Instantiate and add Slim specific extension
    $view->addExtension(new Slim\Views\TwigExtension(
        $c['router'],
        $c['request']->getUri()
    ));

    return $view;
};
```