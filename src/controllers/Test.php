<?php
/**
 * Created by PhpStorm.
 * User: inhere
 * Date: 2016/3/8
 * Time: 17:48
 */

namespace app\controllers;

use inhere\validate\Validation;
use Slim;
use slimExt\base\Request;
use slimExt\base\Response;

class Test extends Base
{
    public function indexAction()
    {

//        de(Slim::$app->settings);

//        $lang = Slim::$app->language;
//        $app = $lang->getOther('app');
//        de($lang, $app->test, $lang->tran('test'));

        return $this->renderTwig('/items/docs/_layouts/gitbooks');

//         $art = new Articles([
//             'authorId' => 1,
//             'title' => 'fffff',
//             'digest' => 'df digest digest digest',
//             'route' => '/blog',
//             'publishTime' => time(),
//             'createTime' => time(),
//         ]);

//         $art->insert();


// de($re);
    }

    public function validateAction()
    {
        $v = Validation::make($this->request->all(), [
            ['tagId,userId,freeTime', 'required', 'msg' => '{attr} is required!'],
            ['tagId', 'size', 'min'=>4, 'max'=>567, 'msg' => '{attr} is must in {min}~{max}!'], // 4<= tagId <=567
            ['name', 'size', 'min'=>2, 'max'=>567, 'msg' => '{attr} is must in {min}~{max}!'], // 4<= tagId <=567
            ['userId', 'int'], // 4<= tagId <=567
            ['userId', function($value){
                return true;
            }, 'msg' => '{attr} is not passed!'],
        ]);

        return $this->response->withJson($v->getErrors());
    }

    public function riotJsAction()
    {
        return $this->renderTwig('/frontend/test/riot-js.twig');
    }
}