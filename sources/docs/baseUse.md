{### 

# start set.开始设置
# (这是注释)设置当前页面的一些信息。
# (this is a comment) set the current page of some information.
# 所有的可设置项请查看文件 'src/config/page.yml'
# All the item can be set up please see the file 'src/config/page.yml'

author: author name
title: 基本使用
route: /docs/baseUse
description: this is page description
keywords: HTML, CSS, JS, JavaScript, framework, bootstrap, front-end
articleTitle: this is article title
tags: [tag1,tag2]
publishTime: 2016-02-23 12:23
prevPage: {title: 文档首页 , url: /docs/index }
nextPage: {title: 开发任务 , url: /docs/dev-task }

# end set. 结束设置
###}

## 页面设置

页面设置 -- 放置在页面最上面，使用 yml 格式，并放置在
开始标记`{###` 和结束标记之间 `###}`。

> 开始、结束标记必须最单独一行

```
author: author name
title: 基本使用
route: /docs/baseUse
description: this is page description
keywords: HTML, CSS, JS, JavaScript, framework, bootstrap, front-end
tags: [tag1,tag2]
publishTime: 2016-02-23 12:23
prevPage: {title: 文档首页 , url: /docs/index }
nextPage: {title: 开发任务 , url: /docs/dev-task }
```

## Test title

test normal text, this is a example page, use markdown syntax.

[test link](/)

> test text

## list

- list 1
- list 1
- list 1


1. list 1
2. list 1
3. list 1

**bold text**

_italic text_

## code

`inline code`

```
// block code
Array(size:10)(
    'lex_var' ⇒ string "/\s*\-\}\}\s*|\s*\}\}/A" (length:23)
    'lex_block' ⇒ string "/\s*(?:\-%\}\s*|\s*%\})\n?/A" (length:28)
    'lex_raw_data' ⇒ string "/(\{%\-|\{%)\s*(?:end%s)\s*(?:\-%\}\s*|\s*%\})/s" (length:48)
    'operator' ⇒ string "/starts\s+with(?=[\s()])|ends\s+with(?=[\s()])|matches(?=[\s()])|not\s+in(?=[\s()])|is\s+not(?=[\s()])|b\-and(?=[\s()])|b\-xor(?=[\s()])|b\-or(?=[\s()])|and(?=[\s()])|not(?=[\s()])|\*\*|\?\?|\.\.|is(?=[\s()])|\/\/|in(?=[\s()])|\<\=|\!\=|\=\=|\>\=|or(?=[\s()])|%|\+|\-|\/|\>|\<|~|\*|\=/A" (length:286)
    'lex_comment' ⇒ string "/(?:\-#\}\s*|#\})\n?/s" (length:22)
    'lex_block_raw' ⇒ string "/\s*(raw|verbatim)\s*(?:\-%\}\s*|\s*%\})/As" (length:43)
    'lex_block_line' ⇒ string "/\s*line\s+(\d+)\s*%\}/As" (length:25)
    'lex_tokens_start' ⇒ string "/(\{\{|\{%|\{#)(\-)?/s" (length:22)
    'interpolation_start' ⇒ string "/#\{\s*/A" (length:9)
    'interpolation_end' ⇒ string "/\s*\}/A" (length:8)
)
```

## table

th | th | th 
---|----|---
tb | tb | tb